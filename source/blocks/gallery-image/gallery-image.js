import Loader from '../loader-image/loader-image';

class Gallery {
  constructor() {
    this.loading = new Loader();
    this.galleryBox = document.querySelector('.gallery-image__box');
    this.info = this.loading.infoStr;
  }

  galleryRender() {
    let div;
    let image;
    let galleryItem;
    this.loading.btn.addEventListener('click', () => {
      let sizeDiff = 0;
      if (this.info.classList.contains('loader-image__info--true')) {
        this.info.classList.remove('loader-image__info--true');
      }
      this.info.innerHTML = '';
      const count = this.galleryBox.childNodes.length;

      if (this.loading.link.value === '' && this.loading.fileJSON.files[0] === undefined) {
        this.info.innerHTML = 'Выберети один из вариантов загрузки!';
        return 0;
      }

      if (this.loading.link.value && this.loading.fileJSON.files[0]) {
        this.info.innerHTML = 'Можно использовать только один из вариантов загрузки!';
        this.loading.link.value = '';
        this.loading.fileJSON.value = '';
        return 0;
      }

      if (this.loading.link.value) {
        setTimeout(() => {
          const src = this.loading.addUrl();
          const exp = new RegExp('^(https?:)?//?[^"<>]+?.(jpg|jpeg|gif|png)$');
          let widthImg = 0;
          let heightImg = 0;
          if (exp.test(src)) {
            div = document.createElement('div');
            image = document.createElement('img');
            this.galleryBox.appendChild(div).classList.add('gallery-image__item');
            galleryItem = document.querySelectorAll('.gallery-image__item');
            galleryItem[count].appendChild(image).setAttribute('src', src);
            galleryItem[count].firstElementChild.classList.add('gallery-image__image');
            widthImg = galleryItem[count].firstElementChild.naturalWidth;
            heightImg = galleryItem[count].firstElementChild.naturalHeight;
            sizeDiff = widthImg / heightImg;
            if ((widthImg > heightImg) && sizeDiff < 1.3) {
              galleryItem[count].classList.add('gallery-image__item--flow-normal');
            }
            if (widthImg === heightImg) {
              galleryItem[count].classList.add('gallery-image__item--flow-normal');
            }
            if ((widthImg > heightImg) && (sizeDiff >= 1.3) && (sizeDiff < 2)) {
              galleryItem[count].classList.add('gallery-image__item--flow-album-medium');
            }
            if ((widthImg > heightImg) && (sizeDiff >= 2)) {
              galleryItem[count].classList.add('gallery-image__item--flow-album-large');
            }
            if (widthImg < heightImg) {
              galleryItem[count].classList.add('gallery-image__item--flow-portrait');
            }
            this.loading.link.value = '';
            setTimeout(() => {
              this.galleryBox.classList.remove('gallery-image__box--disable');
              this.info.classList.add('loader-image__info--true');
              this.info.innerHTML = 'Загрузка выполнена!';
              setTimeout(() => {
                document.documentElement.scrollTop = this.loading.parent.offsetHeight;
              }, 1000);
            }, 1000);
          } else {
            this.info.innerHTML = 'Введите корректный URL-адрес!';
            this.loading.link.value = '';
          }
        }, 1000);
      }

      if (this.loading.fileJSON.files[0]) {
        const success = new Promise((resolve) => {
          this.loading.addFile().then((data) => {
            resolve(data);
          });
        });
        let xhr;
        success.then((file) => {
          xhr = file;
        });
        setTimeout(() => {
          if (this.galleryBox.childNodes.length === 0) {
            xhr.galleryImages.forEach((obj, i) => {
              div = document.createElement('div');
              image = document.createElement('img');
              this.galleryBox.appendChild(div).classList.add('gallery-image__item');
              galleryItem = document.querySelectorAll('.gallery-image__item');
              galleryItem[i].appendChild(image).setAttribute('src', obj.url);
              galleryItem[i].firstElementChild.classList.add('gallery-image__image');
              sizeDiff = obj.width / obj.height;
              if ((obj.width > obj.height) && (sizeDiff < 1.3)) {
                galleryItem[i].classList.add('gallery-image__item--flow-normal');
              }
              if (obj.width === obj.height) {
                galleryItem[i].classList.add('gallery-image__item--flow-normal');
              }
              if ((obj.width > obj.height) && (sizeDiff >= 1.3) && (sizeDiff < 2)) {
                galleryItem[i].classList.add('gallery-image__item--flow-album-medium');
              }
              if ((obj.width > obj.height) && (sizeDiff >= 2)) {
                galleryItem[i].classList.add('gallery-image__item--flow-album-large');
              }
              if (obj.width < obj.height) {
                galleryItem[i].classList.add('gallery-image__item--flow-portrait');
              }
            });
          } else {
            xhr.galleryImages.forEach(() => {
              div = document.createElement('div');
              this.galleryBox.appendChild(div).classList.add('gallery-image__item');
            });
            galleryItem = document.querySelectorAll('.gallery-image__item');
            if (galleryItem.length > count) {
              xhr.galleryImages.forEach((obj, i) => {
                image = document.createElement('img');
                galleryItem[count + i].appendChild(image).setAttribute('src', obj.url);
                galleryItem[count + i].firstElementChild.classList.add('gallery-image__image');
                sizeDiff = obj.width / obj.height;
                if ((obj.width > obj.height) && (sizeDiff < 1.3)) {
                  galleryItem[count + i].classList.add('gallery-image__item--flow-normal');
                }
                if (obj.width === obj.height) {
                  galleryItem[count + i].classList.add('gallery-image__item--flow-normal');
                }
                if ((obj.width > obj.height) && (sizeDiff >= 1.3) && (sizeDiff < 2)) {
                  galleryItem[count + i].classList.add('gallery-image__item--flow-album-medium');
                }
                if ((obj.width > obj.height) && (sizeDiff >= 2)) {
                  galleryItem[count + i].classList.add('gallery-image__item--flow-album-large');
                }
                if (obj.width < obj.height) {
                  galleryItem[count + i].classList.add('gallery-image__item--flow-portrait');
                }
              });
            }
          }
          setTimeout(() => {
            this.galleryBox.classList.remove('gallery-image__box--disable');
            this.info.classList.add('loader-image__info--true');
            this.info.innerHTML = 'Загрузка выполнена!';
            setTimeout(() => {
              document.documentElement.scrollTop = this.loading.parent.offsetHeight;
            }, 1000);
          }, 1000);
          this.loading.fileJSON.value = '';
        }, 1000);
      }
      return 0;
    });
    this.loading.btnRemove.addEventListener('click', () => {
      if (this.galleryBox.childNodes.length === 0) {
        this.info.innerHTML = 'Галерея не создана!';
      } else {
        this.info.innerHTML = '';
        setTimeout(() => {
          galleryItem.forEach((item) => {
            item.remove();
          });
          this.galleryBox.classList.add('gallery-image__box--disable');
          this.info.classList.add('loader-image__info--true');
          this.info.innerHTML = 'Отчистка выполнена!';
        }, 1000);
      }
    });
  }
}

export default Gallery;
