class Loader {
  constructor() {
    this.parent = document.querySelector('.loader-image');
    this.link = document.querySelector('.loader-image__input-url');
    this.fileJSON = document.querySelector('.loader-image__input-file');
    this.btn = document.querySelector('.loader-image__button');
    this.infoStr = document.querySelector('.loader-image__info');
    this.btnRemove = document.querySelector('.loader-image__button--remove');
  }

  addUrl() {
    const urlImage = this.link.value;
    return urlImage;
  }

  addFile() {
    const fileImage = this.fileJSON.files[0];
    const reade = new window.FileReader();
    let dataJSON;
    const delay = new Promise((resolve) => {
      reade.onload = (e) => {
        dataJSON = JSON.parse(e.target.result);
        resolve(dataJSON);
      };
    });
    reade.readAsBinaryString(fileImage);

    return delay;
  }
}

export default Loader;
